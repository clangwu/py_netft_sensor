# Python读取力传感数据包

## 如何使用python读取数据

1. ```
   git clone https://gitee.com/clangwu/py_netft_sensor.git
   ```

2. ```
   python netft.py
   ```

## 如何使用力传感器

#### 硬件接线
![输入图片说明](photo/netft.jpeg)
![输入图片说明](photo/power_connect.jpeg)
1. 红标线为传感器电源线，接学生电源（12-24V）如上图
2. 黄标线为网线，接电脑网口
3. 黑标线为传感器数据线，接力传感器
#### 电脑配置
![输入图片说明](photo/ip_config.png)
断开WIFI，连接有线网，将电脑ip设置为上图