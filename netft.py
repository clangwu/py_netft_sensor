import NetFT 

#0 set ip and connect
netft_ip = "192.168.1.1"
sensor = NetFT.Sensor(netft_ip)

# 1 set data to 0 value
sensor.setbias()

#2 get data
getT = sensor.getNetTorque
getF= sensor.getNetForce

print(getT())
print(getF())
